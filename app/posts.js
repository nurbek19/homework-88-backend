const express = require('express');
const multer = require('multer');
const router = express.Router();
const path = require('path');
const nanoid = require('nanoid');
const Post = require('../models/Post');
const User = require('../models/User');
const config = require('../config');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const createRouter = () => {
    router.get('/', (req, res) => {
       Post.find()
           .then(posts => res.send(posts))
           .catch(error => res.status(400).send(error));
    });

    router.post('/', upload.single('image'), async (req, res) => {
        const token = req.get('Token');
        const post = req.body;

        if (!token) {
            return res.status(401).send({error: 'Token is not found'});
        }

        const user = await User.findOne({token: token});

        if (!user) {
            return res.status(401).send({error: 'User is not authorized'});
        }

        post.user = user._id;

        if (req.file) {
            post.image = req.file.filename;
        } else {
            post.image = null;
        }

        const p = new Post(post);

        await p.save();

        return res.send(p);

    });

    return router;
};

module.exports = createRouter;