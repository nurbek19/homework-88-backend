const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const CommentShema = new Schema({
   description: {
       type: String,
       user: {
           type: Schema.Types.ObjectId,
           ref: 'User',
           required: true
       },
       postId: {
           type: Schema.Types.ObjectId,
           ref: 'Post',
           required: true
       }
   }
});

const Comment = mongoose.model('Comment', CommentShema);

module.exports = Comment;